﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Calculette
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        //Affiche la valeur du bouton sur l'écran
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Button b = (Button)sender;
            if (resultScreen.Text == "Error")
            {
                resultScreen.Text = "";
            }
            resultScreen.Text += b.Content.ToString();
        }

        //Bouton =, lance la fonction solve
        private void Result_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //Affecte le résultat de l'équation à l'écran à une variable
                double result = SolveEquation(resultScreen.Text);
                //Vide l'écran
                resultScreen.Text = "";
                //Affiche le contenu de la variable result sur l'écran
                resultScreen.Text += result;
            }
            catch (Exception)
            {
                resultScreen.Text = "Error";
            }
        }

        //Ac vide l'ecran de la calculette
        private void Ac_Click(object sender, RoutedEventArgs e)
        {
            resultScreen.Text = "";
        }

        //Del supprime le dernier caractère
        private void Del_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                resultScreen.Text = resultScreen.Text.Remove(resultScreen.Text.Length - 1);
            }
            catch (Exception)
            {
                resultScreen.Text = "";
            }
        }

        //Met la valeur en négatif
        private void PosNeg_Click(object sender, RoutedEventArgs e)
        {
            //TODO
            resultScreen.Text += "-";
        }

        //Résout l'équation à l'écran
        public double SolveEquation(string equation)
        {
            Operation operation = new Operation();

            operation.Parse(equation);
            double result = operation.Solve();

            return result;
        }
    }
}
