﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Calculette
{
    class Operation
    {
        //Une operation est composée d'un opérateur et de deux autres opérations de chaque côté de l'opérateur
        private Operation Left;
        private Operation Right;
        private string Operator;

        //Résultat de l'équation
        private double result;

        //Regex groupant les ordres de priorité des opérations
        private Regex addSub = new Regex("[+-]");
        private Regex mulDiv = new Regex("[*/]");
        private Regex percent = new Regex("[%]");

        //Sert à trouver les opérateurs et à créer les sous opérations d'une équation
        public void Parse(string equation)
        {
            var op = addSub.Match(equation);
            //Vérifie si l'opération n'est pas une addition ou soustraction
            if (!op.Success)
            {
                op = mulDiv.Match(equation);
                //Vérifie si l'opération n'est pas une multiplication ou division
                if (!op.Success)
                {
                    op = percent.Match(equation);
                }
            }

            //Si une regex est matchée
            if (op.Success)
            {
                Operator = op.Value;

                //Nouvelle opération avec l'expression de gauche
                Left = new Operation();
                //Relance parse avec l'expression de gauche
                Left.Parse(equation.Substring(0, op.Index));

                //Nouvelle opération avec l'expression de droite
                Right = new Operation();
                //Relance parse avec l'expression de droite
                Right.Parse(equation.Substring(op.Index + 1));
            }
            //Si aucune regex n'est matchée, c'est qu'il n'y a plus d'opérateurs dans l'expression
            else
            {
                //On met une valeur bidon à l'opérateur
                Operator = "x";
                //On convertit donc le résultat en double
                result = double.Parse(equation);
            }
        }

        //Sert à résoudre un calcul en fonction de son opérateur
        //On relance Solve() pour chaque sous-opérations
        public double Solve()
        {
            switch (Operator)
            {
                case "+":
                    result = Left.Solve() + Right.Solve();
                    break;
                case "-":
                    result = Left.Solve() - Right.Solve();
                    break;
                case "*":
                    result = Left.Solve() * Right.Solve();
                    break;
                case "/":
                    result = Left.Solve() / Right.Solve();
                    break;
                case "%":
                    result = Left.Solve() * Right.Solve() / 100;
                    break;
                case "x":
                    break;
            }
            return result;
        }
    }
}
